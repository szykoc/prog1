### Consumer
+ Napisz Consumer który czyści listę stringów

### Predicate 
+ Napisz Predicate który sprawdza czy podany ciąg znaków jest pusty
+ Napisz Predykat który sprawdza czy podany ciąg znaków nie jest pusty i nie jest nullem

------

1. Stwórz klasę Employee z polami : 
  - firstName
  - lastName
  - age
  - profession
  - salary
  - contractType

Następnie zbudujmy listę pracowników z która będziemy pracowali później : 

        List<Employee> employees = List.of(
            new Employee(.....),
            new Employee(.....),
            new Employee(.....)
        );

### Map 

            employees.stream() 
                     .map(...)
                     .forEach(System.out::println)
                     

### Filter 

       
              employees.stream()
                     .map(...)
                     .filter(....)
                     .forEach(System.out::println)
             

### Collectors 

        stream()
            .filter()
            .map()
            .collect(**Collector**)
        

+ toList() - dodaj do listy wszystkich w wieku powyżej 30 lat zarabiających powyżej 2000zł
 (ma powstać nowa list)
 
        stream()
             .collect(toMap(...))
 
+ toMap() - stwórz mapę gdzie kluczem jest nazwisko a wartościa pensja

       stream()
           .map()
           .collect(...) 

+ joining() - wypisz wszystkie imiona oddzielone | 

+ partitioningBy() - użyj w celu podzielenia pracowników na FULL/HALF 

+ groupingBy()

        groupingBy(Function<T,R) -> Collector
        groupingBy(Function<T,R, Collector) -> Collector
        
            stream()
                .collect(groupingBy(....))
        
            stream()
               .collect(groupingBy(...., mapping()))
 
  
    
+ collectingAndThen()

        groupingBy and mapping (Function, Collector)
        collectingAndThen (Collector, Function)
        
        stream()
             .collect(grupingBy(.... , collectingAndThen()))
        
  
+ maxBy()

        stream()
             .collect(maxBy(....))
    
+ filtering()
   
        stream()
             .collect(groupingBy(...., filtering()))
 
 #### Zadanka
  Korzystając ze wczytanej listy pracowników rozwiąż:
  1. Wypisz osoby których wynagorodzenie jest w przedziale 2500-3199
  2. Wypisz osoby o wieku parzystym
  3. Wypisz osoby których nazwisko kończy się na ska i pracują na cały etat
  4. Wypisz imiona wszystkich pracowników
  4. Wypisz 3 ostatnie litery z naziwska, jeśli kończa się na ski/ska maja być z dużych liter  
  5. Wypisz profesje wszystkich pracownikow z dużych liter
  6. Sprawdź czy w liscie instnieje osoba o nazwisku "Kowalski", jedną instrukcją
  7. Zasymuluj podwyżki pracowników o 12%, tworząc wynikową mapę której kluczem będzie
  nazwisko a wartościa wynagrodzenie po podwyżce
  8. Wyfiltruj pracowników tak, aby pozostawić tych których drugi znak imienia to 'a'
     a czwarty nazwiska to 'b'
  9. Posortuj listę osób w następujący sposób - wpierw nazwisko alfabetycznie rosnąco, następnie imię.
  10. Wypisz wszystkie nazwiska w formacie {lastName,lastName}
  11. Wypisz ilość osób zarabiających wiecęj niż 5000, w formacie true->20, false->30 (patritinioningBy() + couting())
  12. Wypisz średnią zarobków osób pracujących na pełny/pół etaptu
  13. Stwórz mapę gdzie kluczem będzie Imię a wartościa List<Employee>
  

    a. Stwórz mapę gdzie kluczem będzie Imię a wartościa liczba jego wystąpienia
    b. Stwórz mapę gdzie kluczem będzie Imię a wartościa liczba jego wystąpienia
    c. Stwórz mapę gdzie kluczem będzie Imię a wartościa liczba jego wystąpienia,
       zbierz imioną tylko te które kończą się na "a"
    d. Stwórz mapę gdzie kluczem będzie Imię a wartościa liczba jego wystąpienia 
       jako Integer

  14. Znajdz osobę najlepiej zarabiająca
  15. Znajdz profesję osoby najgorzej zarabiającej / najlepiej
  16. Wylicz średnia zarobków kobiet
  17. Wylicz średnia zarobków mężczyzn
  18. Pogrupuj osoby po nazwisku, tylko te którch imię zaczyna się na K
  19. Stwórz Mapę której kluczem będzie profesja a kluczem lista wszystkich zarobkow mniejszych niz 2000
      (użyj grupingBy, mapping, filtering)