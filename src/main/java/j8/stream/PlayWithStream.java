package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;

import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.jar.JarOutputStream;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;


public class PlayWithStream {

    public static void main(String[] args) {
        List<Employee> employees = FileUtils.load(Path.of("src/main/resources/j8/employees.csv"));

        // ex1(employees);
        // ex2(employees);
        // ex3(employees);
        // ex4(employees);
        //ex5(employees);
        //ex6(employees);
        //ex7(employees);
        //ex8(employees);
        //ex9(employees);
        //ex10(employees);
        //ex11(employees);
        //ex12(employees);
        // ex13(employees);
        //ex14(employees);
        //ex14a(employees);
        //ex14b(employees);
        ex15(employees);
    }

    private static void ex15(List<Employee> employees) {
        Optional<Employee> collect = employees.stream()
                .collect(
                        minBy(
                                Comparator.comparing(e -> e.getSalary())
                        )
                );

        Optional<Employee> empGooo =
                employees
                        .stream()
                        .filter(e -> e.getFirstName().equals("Kasia"))
                        .findFirst();

        System.out.println(collect);
        boolean present = empGooo.isPresent();
        if (present) {
            Employee employee = empGooo.get();
            System.out.println(employee.getFirstName());
        } else {
            System.out.println("TEGO GOSCIA NIE MA");
        }

        empGooo.ifPresentOrElse(
                e -> System.out.println(e.getFirstName()),
                () -> System.out.println("TEGO NIE MA")
        );

        empGooo.ifPresent(
                e -> e.getFirstName()
        );

        //IntStatistic
        System.out.println(employees.stream()
                .mapToDouble(e -> e.getSalary())
                .summaryStatistics());

        System.out.println(IntStream.of(1, 2, 4, 5, 6)
                .summaryStatistics());
    }

    private static void ex14b(List<Employee> employees) {
        Map<String, Long> a = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getFirstName(),
                                filtering(
                                        e -> e.getFirstName().endsWith("a"),
                                        counting()
                                )
                        )
                );
        System.out.println(a);


    }

    private static void ex14a(List<Employee> employees) {
        Map<String, Integer> collect = employees.stream()
                .collect( // Steam<EMPLOYYEE>
                        groupingBy(
                                Employee::getFirstName, // MAP < STRING, INTA>
                                collectingAndThen(
                                        counting(), // LONG
                                        e -> e.intValue() // LONGA -> INTA
                                )
                        )
                );
        System.out.println(collect);
    }

    private static void ex14(List<Employee> employees) {
        Map<String, List<Employee>> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getFirstName()
                        )
                );
        System.out.println(collect);
    }

    private static void ex13(List<Employee> employees) {
        Map<String, Double> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getType().name(),
                                averagingDouble(
                                        e -> e.getSalary()
                                )
                        )
                );
        System.out.println(collect);
    }

    private static void ex12(List<Employee> employees) {

        Map<Boolean, Long> collect = employees.stream()
                .collect(
                        Collectors.partitioningBy(
                                e -> e.getSalary() > 5000,
                                Collectors.counting()
                        )
                );
        System.out.println(collect);
    }

    private static void ex11(List<Employee> employees) {
        String collect = employees.stream()
                .map(e -> e.getLastName())
                .collect(Collectors.joining("|"));

        System.out.println(collect);
    }

    private static void ex10(List<Employee> employees) {
        employees.stream()
                .sorted(
                        Comparator.comparing(Employee::getSalary)
                                .thenComparing(Employee::getFirstName)
                )
                .forEach(System.out::println);
    }

    private static void ex9(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getFirstName().charAt(2) == 'a')
                .filter(e -> e.getLastName().charAt(2) == 'b')
                .forEach(System.out::println);
    }

    private static void ex8(List<Employee> employees) {

        Map<String, List<Double>> collect =
                employees.stream()
                        .collect(
                                groupingBy(
                                        Employee::getLastName,
                                        mapping(
                                                e -> e.getSalary() * 1.12, toList()
                                        )
                                )
                        );

        System.out.println(collect);
    }

    private static void ex7(List<Employee> employees) {
        boolean isKowlaska =
                employees.stream()
                        .map(Employee::getLastName)
                        .anyMatch(e -> e.equals("Kowalska"));
        System.out.println(isKowlaska);
    }

    private static void ex6(List<Employee> employees) {
        employees.stream()
                //.map(e -> e.getProfession())
                .map(Employee::getProfession)
                //.map(e -> e.toUpperCase())
                .map(String::toUpperCase)
                //.map(e -> e.getProfession().toUpperCase())
                .forEach(System.out::println);
    }

    private static void ex5(List<Employee> employees) {
        employees.stream()
                .map(Employee::getLastName)
                .map(e -> e.substring(e.length() - 3))
                .map(e ->
                        (e.endsWith("ski") || e.endsWith("ska"))
                                ? e.toUpperCase() : e
                )
                .forEach(System.out::println);

    }

    private static void ex4(List<Employee> employees) {
        employees.stream()
                .map(Employee::getFirstName)
                .forEach(System.out::println);
    }

    private static void ex3(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska"))
                .filter(e -> e.getType().equals(ContractType.F))
                .forEach(System.out::println);
    }

    private static void ex2(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getAge() % 2 == 0)
                .forEach(System.out::println);
    }

    private static void ex1(List<Employee> employees) {
        employees.stream()
                .filter(salaryBeetwen2500_3199())
                .forEach(System.out::println);

    }

    private static Predicate<Employee> salaryBeetwen2500_3199() {
        return e -> e.getSalary() > 2500 && e.getSalary() < 3199;
    }
}
