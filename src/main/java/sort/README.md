1. Stwórz interfejs Sorter z metodą sort, przyjmującą jako argument tablicę intów. 
Interfejs oferuje również metodę getAlgorithName, która zwróci nazwę użytego algorytmu do sortowania.

        public interface Sorter {
            void sort(int[] array);
            String getAlgorithmName();
        }
2. Stwórz implementacje tego interfejsu - BubbleSorter, InsertionSorter, SelectionSorter, QuickSorter
            
        public class BubbleSorter implements Sorter {
            ...
        }
            
4. Do klasy ArrayUtils dodaj metodę generateIntegerArray, która zwróci tablicę losowych liczb całkowitych o zadanym rozmiarze.

5. Stwórz listę obiektów typu Sorter - niech zawiera po jednym obiekcie z wszystkich stworzonych implementacji.
Dla losowych tablic o kolejno 1000, 10000 oraz 100000 elementach użyj każdego z sorterów do posortowania.
W każdym przypadku wypisz liczbę elementów tablicy, nazwę użytego algorytmu i czas potrzebny do posortowania.
Podpowiedź: użyj metody System.currentTimeMillis()
Uważaj na to, żeby nie sortować tablicy już posortowanej - twórz kopie tablicy na starcie i sortuj kopie.



-----

##### bubble sort pseudo code

        for i from 1 to N
           for j from 0 to N - 1
              if a[j] > a[j + 1]
                 swap (a[j], a[j + 1])
                 

##### insertion sort pseudo code
        
        for i from 1 to N - 1
          j = i
          while j > 0 and a[j - 1] > a[j]
             swap a[j] and a[j - 1]
             j = j - 1

##### selection sort pseudo code
        for i from 0 to N - 1 
           int iMin = i;
           for (j = i + 1, j < n ; j++)
               if (a[j] < a[iMin])
                  iMin = j;
           if (iMin     != i)
              swap(a[i] a[iMin])
              
##### quick sort pseudo code 

        quickSort(A as array, low as int, high as int)
             if (low < high)
                pivot = partition(A, low, high)
                quickSort(A, low, pivot)
                quickSort(A, pivot + 1, high)
                
        partition(A as array, low as int, high as int)
            pivot = A[low]
            leftwall = low
            
            for i = low + 1 to high
                if (a[i] < pivot) 
                   swap(a[i], a[leftwall])
                   leftwall = leftwall + 1
                   
            swap(pivot, a[leftwall])
            return leftwall
        
              
     