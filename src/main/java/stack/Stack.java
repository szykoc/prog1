package stack;

public class Stack {
    private int array[];
    private int top;
    private int capacity;

    public Stack(int capacity) {
        this.capacity = capacity;
        this.array = new int[capacity];
        this.top = -1;
    }

    public void push(int element){
        if (isFull()){
            System.out.println("Stack is full");
        } else {
            array[++top] = element;
        }
    }

    private boolean isFull() {
        return top == capacity - 1;
    }

    private boolean isEmpty() {
        return top == -1;
    }

    public int pop(){
        if (isEmpty()){
            System.out.println("Stack is empty");
        } else {
            return array[top--];
        }
        return 0;
    }

    public int peek(){
        return array[top];
    }

}
