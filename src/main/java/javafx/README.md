Zadania 1.X, 2.X i 3.X realizujemy w osobnych projektach.

+ 1.1. Stwórz projekt JavaFX, w którym na widoku umieścisz jedno pod drugim: label, pole tekstowe, przycisk oraz label.
+ 1.2. Treść pierwszego labela powinna być pytaniem. W polu tekstowym użytkownik podaje odpowiedź. Przycisk ma tekst "Odpowiedz". Po jego kliknięciu w drugim labelu zostaje wyświetlony komunikat o poprawności odpowiedzi.
+ 2.1. Stwórz nowy projekt o nazwie calculator (projekt javaFX).
Zaprojektuj widok z trzema polami tekstowymi (inputTextField1, inputTextField2, outputTextField), oraz przyciskiem z tekstem +. Aplikacja powinna działać tak, że kliknięcie przycisku + spowoduje wyświetlenie wyniku dodawania wartości z obu pól tekstowych wejściowych w polu wyjściowym.
+ 2.2. Dodaj przyciski -, *, /, które posłużą do wykonywania innych operacji arytmetycznych. Dodatkowo zablokuj możliwość edycji pola wynikowego.
+ 1.3. Stwórz klasę Question, o polach question i answer (łańcuchy znaków)
+ 1.4. Do kontrolera dodaj pola currentQuestionIndex oraz points typu int oraz listę pytań(i odpowiedzi).
Stwórz metodę displayQuestion, która wyświetli aktualne pytanie w widoku.
+ 1.5. Po kliknięciu przycisku "Odpowiedz" zaktualizuj wartość pola points (zlicza poprawne odpowiedzi), przejdź do następnego pytania oraz wywołaj metodę displayQuestion.
+ 1.6. Po odpowiedzeniu na wszystkie pytania zablokuj przycisk oraz wyświetl zamiast pytania treść "uzyskałeś: XXX punktów".
+ 3.1 Stwórz nowy projekt reprezentujący kalkulator - tym razem klasyczny kalkulator. Mamy przyciski:

        C / * -
        7 8 9 +
        4 5 6
        1 2 3 =

Nad przyciskami powinniśmy mieć nieedytowalne pole tekstowe. Widok stwórz za pomocą kontenera typu GridPane.
+  3.2 Kalkulator powinien się zachowywać jak klasyczny kalkulator.
