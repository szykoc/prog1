package hostpital;

// POJO -> plain old java object
public class Patient  {
    private String name;
    private String surname;
    private Integer howAngry;
    private Disease disease;

    public String getName() {
        return name;
    }

    public Patient setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public Patient setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public Integer getHowAngry() {
        return howAngry;
    }

    public Patient setHowAngry(Integer howAngry) {
        this.howAngry = howAngry;
        return this;
    }

    public Disease getDisease() {
        return disease;
    }

    public Patient setDisease(Disease disease) {
        this.disease = disease;
        return this;
    }

}
