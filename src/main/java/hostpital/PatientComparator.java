package hostpital;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {

    private static final String KOWALSKI = "Kowalski";

    // [patient1. patient2, patient3, patient4, patient5]
    // [patient2, patient1,patient3, patient4,]
    // poll()
    // patient1 - w przypadku normalnej kolejki
    // poll()

    @Override
    public int compare(Patient o1, Patient o2) {
        // - 1 zwracamy gdy o1 ma większy priorytet
        // 1  zwracamy gdy o2 ma większy priorytet
        // 0  zwracamy gdy sa rowne
        boolean isFirstKowalski = o1.getSurname().equals(KOWALSKI);
        boolean isSecondKowalski = o2.getSurname().equals(KOWALSKI);

        if (!isFirstKowalski && isSecondKowalski){
            return 1;
        } else if (isFirstKowalski && !isSecondKowalski){
            return -1;
        } else {
            boolean isO1SthSerious = o1.getDisease()
                    .equals(Disease.STH_SERIOUS);
            boolean isO2SthSerious = o2.getDisease()
                    .equals(Disease.STH_SERIOUS);

            if (!isO1SthSerious && isO2SthSerious){
                return 1;
            } else if (isO1SthSerious && !isO2SthSerious) {
                return -1;
            } else {
                Integer o1Factor =
                        o1.getDisease().getInfectiousness()
                        * o1.getHowAngry();
                Integer o2Factor =
                        o2.getDisease().getInfectiousness()
                                * o2.getHowAngry();

                return o2Factor.compareTo(o1Factor);
            }
        }
    }
}
