#### Hospital queue
Aplikacja powinna pozwalać na:

1. Rejestrację nowego pacjenta

2. Wyświetlanie aktualnego stanu kolejki(kto w niej stoi, nieposortowane, jaka jest długa)

3. Pobieranie następnej osoby z kolejki

4. Podglądanie kto jest następny w kolejce

----


I. Utwórz klasę Patient mającą pola: 

         imie(String), nazwisko(String), 
         jakBardzoZly(int), rozpoznanaChoroba(enum z polem zaraźliwosc
         GRYPA(1) PRZEZIEBIENIE(2), BIEGUNKA(3), COS_POWAZNEGO(4)) 
_-pamietaj o getterach, setterach - Java Style i angielskich nazwach_


II. Utwórz klasę HospitalQueueService mającą pole typu PriorityQueue, 
    Utwórz metodę 

    add(Patient) 
    Patient next() -> pobierającą kolejną osobę z kolejki
    Patient peek() -> podglądające kto jest następny
    
    *Metoda next powinna zwracać najpierw osoby o nazwisku “Kowalski” (to nazwisko ordynatora), 
    w następnej kolejności powinna zwracać osoby z czymś poważnym, dalej osoby, 
    których iloczyn jakBardzoZly i zaraźliwość będzie wyższy-zerknij na interfejs Comparable i 
    konstruktor PriorityQueue
    
III. W menu, które stworzysz w Main powinny być trzy pozycje:
    
  + Następny-wywołujące next i wypisujące kto jest następny (i zdejmujące tą osobęz kolejki)
  + Kto następny - wywołujące peek()
  + Nowy pacjent-umożliwiające podanie imienia, nazwiska, złości i rozpoznanej choroby, a następnie wrzucające to na kolejkę

-----

*Nazwisko ordynatora zamiast hardkodować wczytaj z pliku properties

*Nawet nienazwisko, a listę nazwisk, ordynatorów może być wielu-mają pierwszeństwo