package queue;

import java.util.Arrays;

public class ArrayQueue<T> implements Queue<T>{

    private Object[] elements;

    public ArrayQueue() {
        this.elements = new Object[0];
    }

    // []
    // copy -> [,] size = 1 , musze dodac element na początek
                        // a wiec size - 1
    // elements[size - 1] = element;
    @Override
    public void offer(T element) {
        //tworzymy kopie tablicy aby zwiększyć jej rozmiar
        elements = Arrays.copyOf(
                elements, elements.length + 1);
        // dodajemy element na 1 miejscu kolejki
        elements[elements.length - 1] = element;
    }

    @Override
    // pobiera pierwszy element zwraca i usuwa z kolejki
    // [12,45,20] <- poll()
    // 12 - dostaje 1 element
    // [null,45,20] -> CopyOfRange(elements, 1, rozmiar tablicy)
    // [null, 45, 20] -> [45, 20] - tworz się nowa tablica od
                                  // indexu 1
    public T poll() {
        // pobranie 1 elementu z kolejki
        if (checkIfArrayIsEmpty()) return null;
        T element = (T) elements[0];
        elements = Arrays.copyOfRange
                (elements, 1, elements.length);
        return element;
    }
    // DRY  - DONT REPEAT YOURSELF
    @Override
    public T peek() {
        if (checkIfArrayIsEmpty()) return null;
        return (T) elements[0];
    }

    @Override
    public int size() {
        return elements.length;
    }

    private boolean checkIfArrayIsEmpty() {
        if (elements.length == 0) {
            System.out.println("Queue is empty");
            return true;
        }
        return false;
    }
}
