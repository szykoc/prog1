#### Kolejka (ang. queue) 
###### -> ludzie czekających do kasy w sklepie, 

FIFO (ang. First In, First Out) – pierwszy na wejściu, pierwszy na wyjściu.


       
       
                        capacity - 5
              <------------------------------>
       
                     size - 3
    peek           <----------------->  
      ^        -------------------------------
      |        |                             |
      |------- | x1   x2   x3     <----- | ----- offer 5
      |        |                             |
      |        -------------------------------
     pool        ^              ^
     remove      |              |
                         
             head/front      rear/tail


Stwórz interfejs Queue 
 
 + void offer(int element);
 
 + int poll();
 
 + int peek();
 
 + int size();
 
 Następnie klasę ArrayQueue i zaimplementuj metody.
 
 Kolejnie klasę LinkedQueue - gdzie zbudujemy kolejkę na podstawie ogniw
  
        
        5,4,6,2
        
          head
 
        [5, ref]   ----->  [4, ref] ---------> [6, ref]
                                                  |          
                                                  |
        Link                                      |
         - int data                               |
         - Link next                           [2, ref]
         
         
 tips offer: 
   - Sprawdzamy, czy mamy czoło kolejki. 
   - Jeśli nie, to element wstawiany do kolejki utworzy jej czoło.
   - Jeśli mamy czoło kolejki, to przebiegamy w pętli po następnych ogniwach,
   aż dojdziemy do ostatniego. Wstawiany element utworzy ogniwo następujace
   po dotychczas ostatnim.