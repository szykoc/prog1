package queue;

public class LinkedQueue<T> implements Queue<T> {

    // Czoło naszej kolejki
    private Link<T> head;

    @Override
    // sprawdzic czy mamy czolo kolejki
    // jesli nie to ustawić na nim nowy link
    // jesli mamy czolo kolejki to w petli przebiegamy
    // po nastepnych ogniwach az dojedziemy do pustego
    // czyli daj następn ego nic nie zwraca
    // [LINK, LINK, LINK, ] <- offer 50
    // [LINK -> daj nastpnego, LINK -> daj nastpeego, ...]

    public void offer(T element) {
        Link<T> newLink = new Link<>(element);
        if (head == null) {
            // jesli nie mamy czola kolejki to je ustawiamy
            head = newLink;
       } else {
            // mamy czolo, przebiegamy w petli
            // dopoki badane ogniwo ma kolejny element
            // idziemy do "ogona"
            Link<T> link = this.head;
            while (link.getNext() != null){
                // iterujemy az dojdziemy do ogona
                link = link.getNext();
            }
            link.setNext(newLink);
        }
    }

    @Override
    public T poll() {
        // jezeli kolejka jest pusta
        if (head == null){
            System.out.println("Empty queue");
            return null;
        }
        // jezli jest to pobieramy wartosc head'a
        T headValue = head.getValue();
        head = head.getNext();
        return headValue;
    }

    @Override
    public T peek() {
        return  (T) head.getValue();
    }

    @Override
    public int size() {
        return 0;
    }
}
